import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

Vue.filter("countLength", str => {
  return `${str} (${str.length})`;
});

new Vue({
  render: h => h(App)
}).$mount("#app");
