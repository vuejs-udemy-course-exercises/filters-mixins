export const textTransform = {
  computed: {
    reverseTXT() {
      return this.text2
        .split("")
        .reverse()
        .join("");
    },
    countTXT() {
      return `${this.text2} (${this.text2.length})`;
    }
  }
};
